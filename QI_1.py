#!/usr/bin/env python
# coding: utf-8

# # Quantum Information Exercises 1

# The aim of this notebook is to practice with the basic aspects of QiSkit and familiarise oneself with the syntax to biuld simple quantum circuits

# ## Basiscs of Quantum Circuits

# In[1]:


from qiskit import QuantumCircuit, assemble, Aer
from qiskit.visualization import plot_histogram


# In[21]:


# print information about qiskit
import qiskit.tools.jupyter
get_ipython().run_line_magic('qiskit_version_table', '')


# In[2]:


qc_output=QuantumCircuit(8)


# In[3]:


qc_output.measure_all()


# In[4]:


qc_output.draw(initial_state=True)


# We use a quantum simulator for running the circuit multiple times, and then plot the results in a histogram

# In[5]:


sim=Aer.get_backend('aer_simulator')
res=sim.run(qc_output).result()
counts = res.get_counts()
plot_histogram(counts)


# In[6]:


qc_encode = QuantumCircuit(8)
qc_encode.x(7)
qc_encode.draw()


# In[7]:


qc_encode.measure_all()
qc_encode.draw(initial_state=True)


# In[8]:


sim=Aer.get_backend('aer_simulator')
res=sim.run(qc_encode).result()
counts = res.get_counts()
plot_histogram(counts)


# In[9]:


qc = QuantumCircuit(2,2)
qc.x(0)
qc.cx(0,1)
qc.measure(0,0)
qc.measure(1,1)
qc.draw()


# In[10]:


sim=Aer.get_backend('aer_simulator')
res=sim.run(qc).result()
counts = res.get_counts()
plot_histogram(counts)


# We write a (classical) half adder circuit, which performs binary additions (0+0=00, 0+1=01, 1+0=01, 1+1=10)

# In[11]:


qc_ha=QuantumCircuit(4,2)
qc_ha.x(0)
qc_ha.x(1) #setting the input as 11

qc_ha.barrier() #put a barrier in
qc_ha.cx(0,2)
qc_ha.cx(1,2)
qc_ha.ccx(0,1,3) #add Toffoli gate

qc_ha.barrier() #put a barrier in
qc_ha.measure(2,0)
qc_ha.measure(3,1)

qc_ha.draw()


# In[12]:


sim=Aer.get_backend('aer_simulator')
res=sim.run(qc_ha).result()
counts = res.get_counts()
plot_histogram(counts)


# # # Representing Quantum States

# In[13]:


from qiskit import QuantumCircuit, assemble, Aer
from qiskit.visualization import plot_histogram, plot_bloch_vector
from math import sqrt, pi


# We can initialise a state such as q_0>= 1/sqrt(2) |0>+1/sqrt(2) |1>

# In[17]:


initial_state = [0+1.j/sqrt(2), 1/sqrt(2)+0.j]  # Define state |q_0>

qc = QuantumCircuit(1) # Define qc
qc.initialize(initial_state, 0) # Initialize the 0th qubit in the state `initial_state`
qc.draw()
qc.save_statevector() # Save statevector, this is a feature of the simulator, not a real quantum computer
qobj = assemble(qc)
state = sim.run(qobj).result().get_statevector() # Execute the circuit
print(state)           # Print the result


# In[18]:


results = sim.run(qobj).result().get_counts()
plot_histogram(results)


# In[16]:


qc.draw()


# In[ ]:




